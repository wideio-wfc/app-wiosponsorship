#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import os
import datetime
import zipfile
import settings
import json
import bson
import uuid
import grp
import datetime
import subprocess
import time
import shutil
import socket


from django.core.urlresolvers import reverse
#
from django.contrib.auth.models import AbstractUser  # , User

import wioframework.fields as models

from wioframework.amodels import *
from wioframework import decorators as dec
from django.http import HttpResponseRedirect
from wioframework.atomic_ops import atomic_incr

#from wioframework.mongo import mongo_db

from bson.objectid import ObjectId
from django.views.decorators.csrf import csrf_exempt
from django.forms import widgets as bwidgets

from wioframework import widgets as wwidgets
from wioframework import validators


from django.core.validators import MaxLengthValidator


from django.contrib import messages
import wioframework.widgets


@wideio_publishable()
@wideio_owned()
@wideiomodel
class Milestone(models.Model):

    def get_all_references(self, stack):
        if (workpackage) not in stack:
            return [workpackage]
        return []
    date_due = models.DateTimeField()
    description = models.TextField()
    workpackage = models.ForeignKey("sponsorship.Workpackage")


@wideio_publishable()
@wideio_owned()
@wideiomodel
class Deliverable(models.Model):

    def get_all_references(self, stack):
        if (workpackage) not in stack:
            return [workpackage]
        return []
    date_due = models.DateTimeField()
    description = models.TextField()
    workpackage = models.ForeignKey(
        "sponsorship.Workpackage", db_index=True, blank=True, null=True)

    class WIDEIO_Meta:
        permissions = dec.perm_for_logged_users_only
        # form_exclude = ['workpackage'] #developers decide their own work
        # package, orgs decide the deliverables.


@wideio_publishable()
@wideio_owned()
@wideiomodel
class ExpectedDeliverable(models.Model):

    def get_all_references(self, stack):
        if (workpackage) not in stack:
            return [workpackage]
        return []
    # title=models.TextField()
    description = models.TextField()
    #sponsorshipproposition=models.ForeignKey("sponsorship.SponsorshipProposition",db_index=True, blank=True, null=True, related_name="expected_deliverable_set")

    def on_add(self, request):
        sp = SponsorshipProposition.objects.get(
            id=request.GET["sponsorshipproposition"])
        sp.requested_deliverables.add(self)
        return {'$client_signal': 'close_modal()'}

    class WIDEIO_Meta:
        permissions = dec.perm_for_logged_users_only
        # form_exclude = ['workpackage'] #developers decide their own work
        # package, orgs decide the deliverables.


@wideio_publishable()
@wideio_owned()
@wideiomodel
class Workpackage(models.Model):

    def get_all_references(self, stack):
        if (workpackage) not in stack:
            return [SponsorshipReply]
        return []
    date_started = models.DateTimeField()
    date_completed = models.DateTimeField()
    description = models.TextField()
    proposition = models.ForeignKey(
        'sponsorship.SponsorshipReply', db_index=True,null=True)

    class WIDEIO_Meta:
        permissions = dec.perm_for_logged_users_only


@wideio_moderated()
@wideio_publishable()
#@wideio_taggable()
#@wideio_followable()
@wideio_owned()
@wideiomodel
class SponsorshipProposition(models.Model):
    ctrname_s = "sponsorship proposition"
    ctrname_p = "sponsorship propositions"

    question_title = models.TextField(help_text="A short title for your question. NOTE: May be rewritten by our staff team",
                                      advanced_help_text="""
                        +1: A great title
                        Shall explain what the scientists aim to achieve during their research. From the title alone it should be possible to understand what the proposition is about.
                        -1: An unsuccessful title
                        Is not descriptive of the question that is trying to be answered, too general or too ambiguous.

                        +1: NOTE: Your title may be rewritten by our staff team
                        To ensure that your question is easily identifiable by the researchers.
                        """
                                      , max_length=100)

#    hide_company_name = with_advanced_help_text("""
#                        -1: If ticked, your question will be published anonymously
#                        Tick this box if you do not want your organisation to be visible on the published question. 
#                        """)(models.BooleanField(default=False))

    hide_company_name = models.BooleanField(default=False)



    one_line_company_description = models.TextField(help_text="An alternative description of the company.", blank="true", null="true", max_length=140, advanced_help_text="""
                        -1: If you are going to hide your company name, this one-line description will still be visible on the published question
                        This gives the reader important context surrounding the question.
                        """)

    small_question_icon_image = with_advanced_help_text("""
						+1: A great thumbnail image
						Visually symbolisea the most crusical or unique aspect of the question and is easily distinguishable from other questions.
                        -1: An unsuccessful thumbnail image
                        Is unrelated to the question, too broad or too ambiguous.
                        """)(models.ForeignKey('references.Image', null=True, related_name="sponsorship_propositions"))

    full_width_descriptive_image = with_advanced_help_text("""
						+1: A great descriptive image
						May be a diagram explaining how the implementation will be used.
                        -1: An unsuccessful descriptive image
                        Is unrelated to the question, too broad or too ambiguous.
                        """)(models.ForeignKey('references.Image', null=True, related_name="alt_sponsorhip_propositions"))

    detailed_question_description = models.TextField(help_text="A detailed description of the problem space and question scope.", max_length=1500, advanced_help_text="""
     					+1: CONTEXT 
     					+1: WHAT YOUR ORGANISATION IS LOOKING FOR
     					+1: A great question description
						Will give a detailed description of the problem space, possible difficulties and question scope.
                        -1: An unsuccessful question description
                        Is unrelated to the question, too broad or too ambiguous.
                        """)

    intended_impact = models.TextField(help_text="Details about what impact a solution to this question would have.",
                                       db_blank=True,
                                       advanced_help_text="""
						+1: A great intended impact
						Will describe the likely benifits that may be enjoyed if the outcome of this research can be successfully applied.
                        -1: An unsuccessful intended impact
                        Will be unrealistic or over ambitious for the scope of the project.
                        """)

    data = models.TextField(help_text="What is the nature of the data, do you require an NDA, Where will the data be provided (if possible please provide a link)? NOTE: It is not reccomended to submit private or sensitive data.",
                            db_blank=True,
                            advanced_help_text="""

						+1: A great dataset
						Will contain a large sample size of high quality, complete, well organised and labeled data.

						+1: A great data description
						Will contain:
						<ul>
                       		<li>&#8226; The nature of the data</li>
							<li>&#8226; Requires signing an NDA?</li>
							<li>&#8226; Where will the data be provided? (if possible please provide a link)</li>
						</ul>
                        -1: NOTE: It is not reccomended to submit private or sensitive data.
                        """)

    organiser = models.ForeignKey(
        'accounts.Organisation', related_name="sponsorship_propositions", null=True)

    question_category = with_advanced_help_text("""
                        -1: Please ensure the enclosing broader question does not exist before creating a new global category. 
                        This helps researchers to see and correctly locate the questions that are relevent to them and therefore helps you to find an answer to your question.

                        +1: NOTE: Your question may be re-categorised by our staff team
                        To ensure that your question is easily identifiable by the researchers.

                        """)(models.ForeignKey('science.Question', related_name="question_sponsorship"))

    one_line_question_description = models.TextField(
        db_blank=True,
        help_text="Exact nature of the improvement searched",
        advanced_help_text="""
                        +1: A great one line question description
                       	Will provide more detail than your question title alone. It will provide the specific context of the question, and be narrower in scope than that of the question category.
                        -1: An unsuccessful one line question description
                        Does not give the reader any further information or context, or is too broad in scope.
                        """
        , max_length=140)

    # BUDGET
    max_budget = with_advanced_help_text("""
						+1: Max budget is in (GBP &#163;K)
                        """)(models.FloatField(default=1000))

    # IP
    provided_background_IP = models.TextField(db_blank=True, help_text="Is there any IP you will provide in order to complete your proposal?", advanced_help_text="""

                        """)
    allowed_background_IP = models.TextField(db_blank=True, help_text="For what parts of the solution can IP be used that you do not own?", advanced_help_text="""
  
                        """)
    sought_foreground_IP = models.TextField(db_blank=True, help_text="Is there any part of the foreground IP you would like to retain for your useage?" , advanced_help_text="""
    					Described acurrately  what actually you look to protect it must be aligned with the requested deliverable and the sought improvement. 
 
    					+1: Successful IP conditions
    					<ul>
    						<li>&#8226; Commercial rights on the resulting code </li> 
    						<li>&#8226; Copyrights and commercial of the resulting code</li> 
    					</ul>
    					-1: Undesirable and restricive IP conditions
    					<ul>
    						<li>&#8226; We want all the right everything related to the algorithm.</li>
    					</ul>
                        """)

    Rightsto_publish_research_paper = with_advanced_help_text("""

                        """)(models.BooleanField(default=True))

    Name_of_the_sponsor_to_be_mentioned = with_advanced_help_text("""

                        """)(models.BooleanField(default=True))

    Allow_reuse_for_non_commercial_projects = with_advanced_help_text("""

                        """)(models.BooleanField(default=False))

    Open_source_permitted = with_advanced_help_text("""

                        """)(models.BooleanField(default=False))

    Derivated_work_permitted = with_advanced_help_text("""

                        """)(models.BooleanField(default=False))

    requested_deliverables = with_advanced_help_text("""
							+1: Thie minimum deliverables for a project will include:
							<ul>
								<li>&#8226; A 10 page report</li>
								<li>&#8226; A git repository in the case of development of software</li>
								<li>&#8226; One algorithm on WIDE IO</li>
							</ul>

                        """)(models.ManyToManyField("sponsorship.ExpectedDeliverable",db_index=True, blank=True, null=True))
	

 #   requirements=models.TextField(db_blank=True, advanced_help_text="""
 #                       """)



    expected_start_date = with_advanced_help_text("""

                        """)(models.DateTimeField( default=lambda: (datetime.datetime.today() + datetime.timedelta(31))))

    expected_completion_date = with_advanced_help_text("""
						-1: By its nature, research is unpredictable and so this is only a guideline date

                        """)(models.DateTimeField( default=lambda: (datetime.datetime.today() + datetime.timedelta(62))))

    success_metrics = models.TextField(db_blank=True, advanced_help_text="""
						+1: Success metrics
						Success metrics must contain a normal &#34;project management deliverable&#34; and &#34;a metric for the comparison of results&#34; the metric must be a scalar value. 
						-1: Know the risks
						Note that this is is research . There is no warranty in research process, risk must exist to justify r&d tax credit.
                        """)

    selected_provider = with_advanced_help_text("""

                        """)(models.ForeignKey('accounts.UserAccount', null=True, related_name="winner_of_sponsorship"))

    visible_to_all = with_advanced_help_text("""
						+1: By default, everyone on the platform is able to view your question
                        """)(models.BooleanField(help_text="If this is public this will be visible to all", default=True))

    invited_providers = with_advanced_help_text("""

                        """)(models.ManyToManyField('accounts.UserAccount', null=True, related_name="candidate_to_sponsorship"))

    request_full_rights = with_advanced_help_text("""

                        """)(models.BooleanField(default=True))

    request_research_and_development_tax_relief_report = with_advanced_help_text("""
						-1: If you are recieving R&D tax relief, you will require this report
                        """)(models.BooleanField(default=True))

    deposit = models.ForeignKey('payment.Deposit', null=True)

    # result section
    result_algorithm = models.ForeignKey('science.Algorithm', null=True)

    def get_all_references(self, stack):
        return [self.small_question_icon_image,	self.full_width_descriptive_image, self.owner]

    def on_add(self, request):
        if request.user.organisation:
            self.organiser = request.user.organisation
            self.owner = request.user
            messages.info(request, 'Your proposition has been saved')
        else:
            raise Exception(
                "You must be a member or an organisation inorder to submit a sponsorship proposition")
            raise ValidationError(
                "You must be a member or an organisation inorder to submit a sponsorship proposition")
        self.save()

    def on_update(self, request):
        if request.user.organisation:
            self.organiser = request.user.organisation
            self.owner = request.user
            messages.info(request, 'Your proposition has been published')
        else:
            raise Exception(
                "You must be a member or an organisation inorder to submit a sponsorship proposition")
            raise ValidationError(
                "You must be a member or an organisation inorder to submit a sponsorship proposition")
        self.save()

        return {'_redirect': '/sponsorship/sponsorshipproposition/view/' + self.id + '/'}
    



    class WIDEIO_Meta:
        allow_query = ['hide_company_name']

        icon = "ion-university"
        search_enabled = ['question_title', 'hide_company_name']
        form_exclude = ['result_algorithm', 'selected_provider', 'organiser']
        permissions = dec.perm_for_logged_users_only

        widgets = {
            'requested_deliverables':
            (lambda f:
             wioframework.widgets.SubElementsWidget(model=ExpectedDeliverable)
             )

        }
        mandatory_fields = ['question_title', 'question_category', 'small_question_icon_image',
                            'one_line_question_description', 'hide_company_name', 'one_line_company_description']

        form_fieldgroups = [
            ('Overview', ['question_title', 'question_category', 'small_question_icon_image', 'one_line_question_description',
                          'hide_company_name', 'one_line_company_description', 'full_width_descriptive_image']),
            ('Description', ['detailed_question_description',
                             'intended_impact', 'data', 'success_metrics']),
            #('IP', ['provided_background_IP', 'allowed_background_IP', 'sought_foreground_IP', 'Rights_granted_to_publish_results_in_the_form_of_a_research_paper', 'Name_of_the_sponsor_to_be_mentioned_in_all_derivated_code',
            #        'Others_may_use_the_resulting_algorithm_for_non_commercial_projects', 'Rights_granted_to_publish_the_algorithm_as_open_source', 'Rights_to_create_derivated_work_from_open_source_code', 'request_full_rights']),
            ('Delivery', ['expected_start_date', 'expected_completion_date', 'max_budget',
                          'requested_deliverables', 'request_research_and_development_tax_relief_report']),
            ('Providers', ['visible_to_all', 'invited_providers']),
        ]
	


@wideio_publishable()
#@wideio_taggable()
#@wideio_followable()
@wideio_owned()
@wideiomodel
class SponsorshipReply(models.Model):
    ctrname_s = "sponsorship reply"
    ctrname_p = "sponsorship replies"

    def get_all_references(self, stack):
        if (workpackage) not in stack:
            return [SponsorshipReply]

        return []

    proposition=models.ForeignKey('sponsorship.SponsorshipProposition',null=True,db_index=True,related_name="replies")

    is_public=models.BooleanField(help_text="If set to public, this submission will be visible to all",default=False)
    
    summary=models.TextField(help_text="A three sentence summary of your research.",app_blank=False,db_blank=False, advanced_help_text="""
                    +1: A great summary
                    Will give an overview of your approach within three or less sentences.
                    -1: An unsuccessful summary
                    Will be too vague, too complex, or be too long.  
                    """)
    
    plan=models.TextField(help_text="What you suggest to do during the research time",app_blank=False,db_blank=False, advanced_help_text="""
                    +1: A great plan
                    Will give an outline of what you believe is realistic and achievable each each stage.
                    """)
    
    innovation=models.TextField(help_text="Why is your solution innovative and why can it outperform others?",app_blank=False,db_blank=False, advanced_help_text="""
                    +1: A great innovation summary
                    Will give an overview of the technology or approach that is different and why you believe this will achieve successful results.
                    """)
    
    team=models.TextField(help_text="Who is in your team and why are they well-suited to this challenge?", advanced_help_text="""
                    +1: A great team summary
                    Will include the names, positions and references of each team member. Also please provide links.
                    """)
    
    required_background_ip=models.TextField(help_text="Is there any background IP that you own that you plan to use? How will this be licenced to the sponsor?", advanced_help_text="""

                    """)
    
    provided_background_ip=models.TextField(help_text="Is there any IP from the sponsor that you require access to in order to complete your proposal?", advanced_help_text="""


                    """)                
    
    foreground_ip_claimed=models.TextField(help_text="Is there any part of the foreground IP you would like to retain for your useage?", advanced_help_text="""
                    Described acurrately  what actually you look to protect it must be aligned with the requested deliverable and the sought improvement. 

                    +1: Successful IP conditions may be:
                    <ul>
                        <li>&#8226; Request to retain the right to use the results as a basis for further research </li> 
                        <li>&#8226; Request to retain the right to use the results for publishing in a scientific paper </li> 
                        <li>&#8226; Request to retain the right to use the resulting algorithm for non-profit open-source projects </li> 
                    </ul>
                    -1: Undesirable and restricive IP conditions
                    <ul>
                        <li>&#8226; Request to retain the right everything related to the algorithm.</li>
                    </ul>    
                    """)
    


    result_algorithm=models.ForeignKey('science.Algorithm',null=True)
    #workpackages=models.ManyToManyField('Workpackage')


    def on_add(self, request):
        messages.info(request, 'Your proposition has been saved and published')
        self.save()
        return {'_redirect': '/sponsorship/sponsorshipreply/view/' + self.id + '/'}

    class WIDEIO_Meta:
        permissions = dec.perm_for_logged_users_only
        # mandatory_fields=['plan'] #< applies only to add

        form_exclude = ['is_public']

        form_fieldgroups = [
            ('Summary', ['summary', 'innovation', 'plan', 'team', 'proposition']),
            ('IP', ['required_background_ip',
                    'provided_background_ip', 'foreground_ip_claimed'])
        ]
