#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import os
import datetime
import zipfile
import settings
import json
import bson
import uuid
import grp
import datetime
import subprocess
import time
import shutil
import socket


from django.core.urlresolvers import reverse
#
from django.contrib.auth.models import AbstractUser  # , User

import wioframework.fields as models

from wioframework.amodels import *
from wioframework import decorators as dec
from django.http import HttpResponseRedirect
from wioframework.atomic_ops import atomic_incr

#from wioframework.mongo import mongo_db

from bson.objectid import ObjectId
from django.views.decorators.csrf import csrf_exempt
from django.forms import widgets as bwidgets

from wioframework import widgets as wwidgets
from wioframework import validators


from django.core.validators import MaxLengthValidator


from django.contrib import messages
import wioframework.widgets


#@wideio_publishable()
@wideio_moderated()
@wideio_owned()
@wideiomodel
class TechCompanyFundingRound(models.Model):
    """
    Do you have a technology - and are you looking to raise fund 
    to reach the next step ?
    """
    def get_all_references(self, stack):
        if (workpackage) not in stack:
            return [workpackage]
        return []

    
    date_round_starts = models.DateTimeField()
    date_round_completes = models.DateTimeField()
    
    # THE IDEA, WHAT, WHAT'S NEW, WHAT DOES IT CHANGES
    proposal_idea = models.TextField()
    technology = models.TextField() # you own
    unfair_advantage = models.TextField(max_length=1000) # coming from tech
    
    global_market=models.TextField(max_length=500)
    
    bootstrapping_strategy=models.TextField(max_length=2000)    
    competitive_advantage=models.TextField(max_length=2000)        

    # ##
    # ## MODEL 0 (RAISING CO-FOUNDERS)
    # ##
    # equity_avalaible = models.FloatField() # coming from tech    
    # min_value = models.FloatField() # coming from tech            
    ##
    ## MODEL 1 (RAISING MONEY)
    ##
    
    equity_available = models.FloatField() # coming from tech    
    min_value = models.FloatField() # coming from tech        
    
    def get_required_valuation(self):
        return self.min_value/self.equity_available
    
    
    def get_total_investment(self):
        from django.db.models import Sum
        return self.investments.all().aggregate(Sum('investment_value'))

    
    
    #proposition = models.ForeignKey('sponsorship.SponsorshipReply', db_index=True,null=True)
    class WIDEIO_Meta:
        permissions = dec.perm_for_logged_users_only
        class Actions:
            pass
            @wideio_action()
            def invest(self,request,mimetype="text/html"):
                return  {'_redirect':TechCompanyFundingRoundInvestment.get_add_url()}
        
        
        
#@wideio_publishable()
@wideio_owned()
@wideiomodel
class TechCompanyFundingRoundInvestment(models.Model):
    """
    Technology looking for fund / cofounder
    """
    models.ForeignKey(TechCompanyFundingRound,db_index=True,related_name="investments")
    
    request_extra_equity=models.BooleanField(default=False,db_index=True) # < THIS IS SEEN AS EXTERNAL TO THE SCOPE OF THE ROUND AND REQUIRES THE COMPANY TO ISSUE EXTRA EQUITY
    is_cash_based=models.BooleanField(default=True,db_index=True)         # < THIS IS SEEN AS EXTERNAL TO THE SCOPE OF THE ROUND AND REQUIRES THE COMPANY TO ISSUE EXTRA EQUITY    
    investment_value=models.FloatField(default=0,db_index=True)
    is_approved=models.BooleanField(default=False,db_index=True)           # < THIS IS SEEN AS EXTERNAL TO THE SCOPE OF THE ROUND AND REQUIRES THE COMPANY TO ISSUE EXTRA EQUITY    
    
    def can_update(self,request):
        return request.user.is_staff
    
    class WIDEIO_Meta:
        permissions = dec.perm_for_logged_users_only        

