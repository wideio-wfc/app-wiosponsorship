#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import os
import datetime
import zipfile
import settings
import json
import bson
import uuid
import grp
import datetime
import subprocess
import time
import shutil
import socket


from django.core.urlresolvers import reverse
#
from django.contrib.auth.models import AbstractUser  # , User

import wioframework.fields as models

from wioframework.amodels import *
from wioframework import decorators as dec
from django.http import HttpResponseRedirect
from wioframework.atomic_ops import atomic_incr

#from wioframework.mongo import mongo_db

from bson.objectid import ObjectId
from django.views.decorators.csrf import csrf_exempt
from django.forms import widgets as bwidgets

from wioframework import widgets as wwidgets
from wioframework import validators


from django.core.validators import MaxLengthValidator


from django.contrib import messages
import wioframework.widgets


##
## COMPANY RAISING TECH WITHOUT FUND
## 
# PROTECTABLE BY NDA
#@wideio_publishable()
@wideio_owned()
@wideiomodel
class TechBid(models.Model):
    """
    Startup looking for scientists to suggest solutions in exchange for equity in their businesses.
    We assume startup have no cash to by IP but still be able to offer something in exchange of the technology.
    """
    date_round_completes = models.DateTimeField()
    
    ## THE PITCH OF THE COMPANY
    proposal_idea =with_advanced_help_text('Describes what you are trying to achieve with technology. Why you think technology or scientific innovation may help you to solve your problem')(models.TextField())
    competitive_advantage_sought = models.TextField('Clarify the metric aspects on how you plan to evaluate') 
    
    ## WHAT THE COMPANIES REQUIRES
    require_algorithm_demo = models.BooleanField(default=True)       # IF ACCEPTED ARE YOU REQUIRED TO PROVIDE A DEMO AS PART OF YOUR PROPOSAL
    require_algorithm_ip = models.BooleanField(default=True)         # IF ACCEPTED ARE YOU REQUIRED TO HANDLE THE ALGORITHM IP TO THE BIDDER
    
    ## REWARD AVAILABLE    
    #is_upfront_cash_available  = models.BooleanField(default=False)    
    upfront_cash_available = models.FloatField(null=True,db_index=True) # coming from tech
    #is_equity_available  = models.BooleanField(default=True)    
    equity_available = models.FloatField(null=True, db_index=True) # coming from tech    
    #is_role_available  = models.BooleanField(default=False)
    role_available = models.TextField(null=True,db_index=True) # role available for student providing solution

    awarded_reply=models.ForeignKey('TechBidReply')
    
    class WIDEIO_Meta:
        form_exclude=["awarded_reply"]
        permissions = dec.perm_for_logged_users_only        

        
        
#@wideio_publishable()
@wideio_owned()
@wideiomodel
class TechBidReply(models.Model):
    """
    Startup looking for scientists to suggest solutions in exchange for equity in their businesses.
    We assume startup have no cash to by IP but may still be 
    """    
    ## THE PITCH OF THE COMPANY
    principle_of_the_technology_offer =with_advanced_help_text('Describe what you are able to achieve with technology. Explain how it solves the problem ?')(models.TextField())
    proofs = models.TextField('Provide elements that allow the company to check that your claims are true ?') 
    
    ## WHAT THE COMPANIES REQUIRES
    has_algorithm_demo = models.BooleanField(default=True)       # IF ACCEPTED ARE YOU REQUIRED TO PROVIDE A DEMO AS PART OF YOUR PROPOSAL

    ## REWARED AVAILABLE    
    is_upfront_cash_requested  = models.BooleanField(default=False)    
    upfront_cash_requested = models.FloatField() # coming from tech
    is_equity_requested  = models.BooleanField(default=True)    
    equity_requested = models.FloatField() # coming from tech    
    is_role_requested  = models.BooleanField(default=False)
    role_requested = models.TextField() # role available for student providing solution

    class WIDEIO_Meta:
        permissions = dec.perm_for_logged_users_only        
        

